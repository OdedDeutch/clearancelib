/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.common;

/**
 * TerminalInfo is used to store the terminal information used to access the Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TerminalInfo {
    final private String serviceName;
    final private String terminalNumber;
    final private String userName;
    final private String password;
    final private String url;
    
    /**
     * Create a new instant of TerminalInfo to be used in the Payment object with the Pelecard API.
     * @param serviceName The <b>serviceName</b> variable.
     * @param terminalNumber The <b>terminalNumber</b> variable.
     * @param userName The <b>userName</b> variable.
     * @param password The <b>password</b> variable.
     * @param url Pelecard url address.
     */
    public TerminalInfo(String serviceName, String terminalNumber, String userName, String password, String url) {
        this.serviceName = serviceName;
        this.terminalNumber = terminalNumber;
        this.userName = userName;
        this.password = password;
        this.url = url;
    }
    
    /**
     * Get a tester pelecard details for testing only.
     * @return a testing TerminalInfo obj.
     */
    public static TerminalInfo getTesterTerminal() {
        return new TerminalInfo("Pelecard test", "0962210", "Lsrtest123", "D4G56hE5", "https://gateway20.pelecard.biz/services");
    }

    /**
     * Get the name of the needed clearance service.
     * @return the name of the service
     */
    public String getServiceName() {
        return serviceName;
    }
    
    /**
     * Get the Pelecard webservice url address.
     * @return The url address.
     */
    public String getUrl() {
        return url;
    }
    
    /**
     * Get a string object containing the terminal number of the current payment.
     * @return The <b>terninalNumber</b> variable.
     */
    public String getTerminalNumber() {
        return terminalNumber;
    }

    /**
     * Get a string object containing the user name of the current terminal.
     * @return The <b>userName</b> variable.
     */
    public String getUserName() {
        return userName;
    }    
    /**
     * Get a string object containing the password of the current terminal.
     * @return The <b>password</b> variable.
     */
    public String getPassword() {
        return password;
    }
}
