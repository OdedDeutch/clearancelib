/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.common;

import isr.commonTools.StringEx;
import isr.commonTools.JSONLogger;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public abstract class ClearingService {
    protected final String vehicleId;
    protected final String manOrSwipe;
    protected final String drNum;
    protected final String cardNum; 
    protected final String expDate; 
    protected final String cvc; 
    protected String cardNumDec; 
    protected String expDateDec; 
    protected String cvcDec; 
    protected final String sum; 
    protected final ActionType type;
    protected final String pNum;
    protected final String subPayload; 
    protected final String refNum;
    protected final String token;
    protected final String id;
    protected String statusCode;
    protected String authNumebr;
    protected String cardLastDigits;
    protected String cardBrand;
    protected String paramX;
    protected JSONLogger jlooger;
    protected boolean debug;
    protected String rootFolder;

    public ClearingService(String vehicleId, String manOrSwipe, String drNum, 
            String cardNum, String expDate, String cvc, String sum, String type, 
            String pNum, String subPayload, String refNum, 
            String token, String id, String paramX, boolean debug, String rootFolder) {
        this.vehicleId = vehicleId;
        this.manOrSwipe = manOrSwipe;
        this.drNum = drNum;
        this.cardNum = cardNum;
        this.expDate = expDate;
        this.cvc = cvc;
        this.sum = sum;
        this.type = ActionType.convert(type);
        this.pNum = pNum;
        this.subPayload = subPayload;
        this.refNum = refNum;
        this.token = token;   
        this.id = id;
        cardLastDigits = StringEx.EMPTY;
        this.paramX = paramX;
        this.debug = debug;  
        this.rootFolder = rootFolder;
    }
    
    public void commitPayment() throws Exception {       
        if(manOrSwipe.equals("0")) {
            cardNumDec = StringEx.decrypt(cardNum, token);
            expDateDec = StringEx.decrypt(expDate, token);
            cvcDec = StringEx.decrypt(cvc, token);
        } else {
            cardNumDec = StringEx.decrypt(cardNum, token);
            expDateDec = StringEx.EMPTY;
            cvcDec = StringEx.EMPTY;
        }  
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getAuthNumebr() {
        return authNumebr;
    }
    
    public String getCardLastDigits() {
        return cardLastDigits;
    }
}
