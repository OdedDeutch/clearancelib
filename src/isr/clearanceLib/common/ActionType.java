/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.common;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum ActionType {
    Regular(1), Payments(2), Refund(3);
    
    private final int type;
    
    private ActionType(int type) {
        this.type = type;
    }
    
    public static ActionType convert(String val) {
        switch(val) {
            case("1"):
                return Regular;
            case("2"):
                return Payments;
            case("3"):
            return Refund;
        }
        return null;
    }
}
