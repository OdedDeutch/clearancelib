/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.common;

import isr.clearanceLib.pelecard.ops.PelecardService;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class ClearingServiceFactory {
    
    public static ClearingService get(String name, TerminalInfo info,
            String vehicleId, String manOrSwipe, String drNum, String cardNum, 
            String expDate, String cvc, String sum, String type, String pNum, 
            String subPayload, String refNum, String token, String id, String paramX, 
            boolean debug, String rootFolder) {
        switch(name) {
            case "Pelecard":
            case "Pelecard test":
                return new PelecardService(info, vehicleId, manOrSwipe, drNum, cardNum, expDate, cvc, sum, type, pNum, 
                        subPayload, refNum, token, id, paramX, debug, rootFolder);    
        }
        return null;
    }
}
