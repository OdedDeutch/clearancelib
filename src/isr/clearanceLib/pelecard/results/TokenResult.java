/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The TokenResult class is holding the data received from Pelecard webservice from the "ConvertToToken" method.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TokenResult extends Result{
    final private TokenResultData tokenResultData;
    
    /**
     * Create a new intent of TokenResult.
     * @param json the Json object received from Pelecard.
     * @throws JSONException 
     */
    public TokenResult(JSONObject json) throws JSONException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData"))
            this.tokenResultData = new TokenResultData(json.getJSONObject("ResultData"));
         else 
            tokenResultData = null;
        
    }

    /**
     * Get the Result data object.
     * @return a TokenResultData object.
     */
    public TokenResultData getTokenResultData() {
        return tokenResultData;
    }
}
