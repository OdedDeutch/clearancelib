/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */

package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The TamalData object holds the data 
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TamalData {
    final private String status;
    final private String link;
    final private String number;
    final private String error;

    /**
     * Create a new intent of TamalData basked on the json received from Pelecard API.
     * @param json the Json object containing the data.
     * @throws JSONException 
     */
    public TamalData (JSONObject json) throws JSONException {
        status = json.getString("InvoiceStatus");
        link = json.getString("InvoiceLink");
        number = json.getString("InvoiceNumber");
        error = json.getString("InvoiceError");
    }
    
    /**
     * Get Tamal status action
     * @return The <b>InvoiceStatus</b> variable.
     */
    public String getInvoiceStatus() {
        return status;
    }

    /**
     * Get the URL of the invoice issued by Tamal.
     * @return The <b>InvoiceLink</b> variable.
     */
    public String getInvoiceLink() {
        return link;
    }

    /**
     * Get number of the invoice issued by Tamal
     * @return The <b>InvoiceNumber</b> variable.
     */
    public String getInvoiceNumber() {
        return number;
    }

    /**
     * Get Tamal's error code, if an error have been accrued.
     * @return The <b>InvoiceStatus</b> variable.
     */
    public String getInvoiceError() {
        return error;
    }
}
