/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import isr.clearanceLib.pelecard.definitions.CallReason;
import isr.clearanceLib.pelecard.definitions.CreditType;
import isr.clearanceLib.pelecard.definitions.DebitType;
import isr.clearanceLib.pelecard.definitions.ApprovedBy;
import isr.clearanceLib.pelecard.definitions.CreditCompany;
import isr.clearanceLib.pelecard.definitions.AbroadCard;
import isr.clearanceLib.pelecard.definitions.CreditCardBrand;
import isr.clearanceLib.pelecard.definitions.DebitCode;
import isr.clearanceLib.pelecard.definitions.Currency;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The ResultData object is a sub-class of the Result class. 
 * It contains the result data of a given request to the Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public abstract class ResultData {
    final private String voucherId;
    final private String shvaResult;
    final private String shvaFileNumber;
    final private String stationNumber;
    final private String reciept;
    final private String jparam;
    final private String creditCardNumber;
    final private String creditCardExpDate;
    final private CreditCompany creditCardCompanyClearer;
    final private CreditCompany creditCardCompanyIssuer;
    final private int creditCardStarsDiscountTotal;
    final private CreditType creditType;
    final private AbroadCard creditCardAbroadCard;
    final private DebitType debitType;
    final private DebitCode debitCode;
    final private int debitTotal;
    final private String debitApproveNumber;
    final private Currency debitCurrency;
    final private int totalPayments;
    final private int firstPaymentTotal;
    final private int fixedPaymentTotal;
    final private String additionalDetailsParamX;
    final private String shvaOutput;
    final private String cardHebName;
    final private CreditCardBrand creditCardBrand;
    final private ApprovedBy approvedBy;
    final private CallReason callReason;

    /**
     * Create a new intent of ResultData object.
     * @param json the json that have been received from Pelecard webservice.
     * @throws JSONException Un exception in the Json data.
     */
    public ResultData(JSONObject json) throws JSONException {       
        this.voucherId = json.getString("VoucherId");
        this.shvaResult = json.getString("ShvaResult");
        this.shvaFileNumber = json.getString("ShvaFileNumber");
        this.stationNumber = json.getString("StationNumber");
        this.reciept = json.getString("Reciept");
        this.jparam = json.getString("JParam");
        this.creditCardNumber = json.getString("CreditCardNumber");
        this.creditCardExpDate = json.getString("CreditCardExpDate");
        this.creditCardCompanyClearer = CreditCompany.convert(json.getInt("CreditCardCompanyClearer"));
        this.creditCardCompanyIssuer = CreditCompany.convert(json.getInt("CreditCardCompanyIssuer"));
        this.creditCardStarsDiscountTotal = json.getInt("CreditCardStarsDiscountTotal");
        this.creditType = CreditType.convert(json.getInt("CreditType"));
        this.creditCardAbroadCard = AbroadCard.convert(json.getInt("CreditCardAbroadCard"));
        this.debitType = DebitType.convert(json.getInt("DebitType"));
        this.debitCode = DebitCode.convert(json.getInt("DebitCode"));
        this.debitTotal = json.getInt("DebitTotal");
        this.debitApproveNumber = json.getString("DebitApproveNumber");
        this.debitCurrency = Currency.convert(json.getInt("DebitCurrency"));
        this.totalPayments = json.getInt("TotalPayments");
        this.firstPaymentTotal = json.getInt("FirstPaymentTotal");
        this.fixedPaymentTotal = json.getInt("FixedPaymentTotal");
        this.additionalDetailsParamX = json.getString("AdditionalDetailsParamX");
        this.shvaOutput = json.getString("shvaOutput");
        this.cardHebName = json.getString("CardHebName");
        this.creditCardBrand = CreditCardBrand.convert(json.getInt("CreditCardBrand"));
        this.approvedBy = ApprovedBy.convert(json.getInt("ApprovedBy"));
        this.callReason = CallReason.convert(json.getString("CallReason"));
    }
   
    /**
     * Get the voucher Id of the request.
     * @return The <b>VoucherId</b> variable.
     */
    public String getVoucherId() {
        return voucherId;
    }

    /**
     * Get the Shva status code of the request.
     * @return The <b>ShvaResult</b> variable.
     */
    public String getShvaResult() {
        return shvaResult;
    }

    /**
     * Get the Shva file number of the request.
     * @return The <b>ShvaFileNumber</b> variable.
     */
    public String getShvaFileNumber() {
        return shvaFileNumber;
    }

    /**
     * Get the station number code of the request from 1 to 999.
     * @return The <b>StationNumber</b> variable.
     */
    public String getStationNumber() {
        return stationNumber;
    }
    
    /**
     * Get the file record number  of the  of the request.
     * @return The <b>Reciept</b> variable.
     */
    public String getReciept() {
        return reciept;
    }

    /**
     * Get the action type of the request.
     * @return The <b>Jparam</b> variable.
     */
    public String getJparam() {
        return jparam;
    }

    /**
     * Get the masked number of the credit card.
     * @return The <b>CreditCardNumber</b> variable.
     */
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * Get the expired date of the credit card, in a MMYY format.
     * @return The <b>CreditCardExpDate</b> variable.
     */
    public String getCreditCardExpDate() {
        return creditCardExpDate;
    }

    /**
     * Get the credit card clearing company.
     * @return The <b>CreditCardCompanyClearer</b> variable.
     */
    public CreditCompany getCreditCardCompanyClearer() {
        return creditCardCompanyClearer;
    }

    /**
     * Get the credit card issuing company.
     * @return The <b>CreditCardCompanyIssuer</b> variable.
     */
    public CreditCompany getCreditCardCompanyIssuer() {
        return creditCardCompanyIssuer;
    }

    /**
     * Get the total discount of the deal.
     * @return The <b>CreditCardStarsDiscountTotal</b> variable.
     */
    public int getCreditCardStarsDiscountTotal() {
        return creditCardStarsDiscountTotal;
    }

    /**
     * Get the credit type of the deal.
     * @return The <b>CreditType</b> variable.
     */
    public CreditType getCreditType() {
        return creditType;
    }

    /**
     * Get the Abroad type of the card.
     * @return The <b>CeditCardAbroadCard</b> variable.
     */
    public AbroadCard getCreditCardAbroadCard() {
        return creditCardAbroadCard;
    }

    /**
     * Get the debit type of the deal.
     * @return The <b>DebitType</b> variable.
     */
    public DebitType getDebitType() {
        return debitType;
    }

    /**
     * Get the debit code of the deal.
     * @return The <b>DebitCode</b> variable.
     */
    public DebitCode getDebitCode() {
        return debitCode;
    }

    /**
     * Get the total sum of the deal, in agorot.
     * @return The <b>DebitTotal</b> variable.
     */
    public int getDebitTotal() {
        return debitTotal;
    }

    /**
     * Get the approve number of the deal.
     * @return The <b>DebitApproveNumber</b> variable.
     */
    public String getDebitApproveNumber() {
        return debitApproveNumber;
    }

    /**
     * Get the currency of the deal.
     * @return The <b>DebitCurrency</b> variable.
     */
    public Currency getDebitCurrency() {
        return debitCurrency;
    }

    /**
     * Get the number of payments of the deal.
     * @return The <b>TotalPayments</b> variable.
     */
    public int getTotalPayments() {
        return totalPayments;
    }

    /**
     * Get the sum of the first payment, in agorot.
     * @return The <b>FirstPaymentTotal</b> variable.
     */
    public int getFirstPaymentTotal() {
        return firstPaymentTotal;
    }

    /**
     * Get the sum of the payments after the first.
     * @return The <b>FixedPaymentTotal</b> variable.
     */
    public int getFixedPaymentTotal() {
        return fixedPaymentTotal;
    }

    /**
     * Get the additional details ParamX of the request.
     * @return The <b>AdditionalDetailsParamX</b> variable.
     */
    public String getAdditionalDetailsParamX() {
        return additionalDetailsParamX;
    }

    /**
     * Get the shva response string of the request.
     * @return The <b>ShvaOutput</b> variable.
     */
    public String getShvaOutput() {
        return shvaOutput;
    }

    /**
     * Get the card's name in Hebrew of the request.
     * @return The <b>CardHebName</b> variable.
     */
    public String getCardHebName() {
        return cardHebName;
    }

    /**
     * Get the brand of the card.
     * @return The <b>CreditCardBrand</b> variable.
     */
    public CreditCardBrand getCreditCardBrand() {
        return creditCardBrand;
    }

    /**
     * Get the identity of the approver of the request.
     * @return The <b>ApprovedBy</b> variable.
     */
    public ApprovedBy getApprovedBy() {
        return approvedBy;
    }

    /**
     * Get the reason code of the request.
     * @return The <b>CallReason</b> variable.
     */
    public CallReason getCallReason() {
        return callReason;
    }   
}
