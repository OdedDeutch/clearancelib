/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TransResultData extends TransactionResultData {

    final private Date createDate;
    final private Date broadcastDate;
    final private String broadcastNo;
    
    /**
     * 
     * @param json
     * @throws JSONException
     * @throws ParseException 
     */
    public TransResultData(JSONObject json) throws JSONException, ParseException {
        super(json);
        
        DateFormat format = new SimpleDateFormat("dd/MM/YYYY HH:mm");
        this.createDate = format.parse(json.getString("CreateDate"));
        this.broadcastDate = format.parse(json.getString("BroadcastDate"));
        this.broadcastNo = json.getString("BroadcastNo");
    }

    /**
     * 
     * @return 
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 
     * @return 
     */
    public Date getBroadcastDate() {
        return broadcastDate;
    }

    /**
     * 
     * @return 
     */
    public String getBroadcastNo() {
        return broadcastNo;
    }   
}
