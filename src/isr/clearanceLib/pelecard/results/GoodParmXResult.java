/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONObject;

/**
 * The GoodParmXResult class is holding the data received from Pelecard webservice from the "ChkGoodParamX" method.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class GoodParmXResult extends Result {
    
    final private TransResultData resultData;
    
    /**
     * 
     * @param json
     * @throws Exception 
     */
    public GoodParmXResult(JSONObject json) throws Exception {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        if(json.has("ResultData"))
            this.resultData = new TransResultData(json.getJSONObject("ResultData"));
        else
            resultData = null;
    }

    /**
     * 
     * @return 
     */
    public TransResultData getResultData() {
        return resultData;
    }
}
