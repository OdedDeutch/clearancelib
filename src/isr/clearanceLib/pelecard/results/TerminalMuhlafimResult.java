/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import java.text.ParseException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TerminalMuhlafimResult extends Result{

    final private TerminalMuhlafimResultData[] resultData;
    
    public TerminalMuhlafimResult(JSONObject json) throws JSONException, ParseException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData")) {
            JSONArray objectArray = json.getJSONArray("ResultData");
            resultData = new TerminalMuhlafimResultData[objectArray.length()];
            for(int idx = 0; idx < resultData.length; idx++ ){
                resultData[idx] = new TerminalMuhlafimResultData(objectArray.getJSONObject(idx));
            }
        } else {
            resultData = null;
        }
    }

    public TerminalMuhlafimResultData[] getResultData() {
        return resultData;
    }
    
    
    
}
