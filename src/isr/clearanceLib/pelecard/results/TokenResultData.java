/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The TokenResultData is in use by the ConverToToken method in the Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TokenResultData extends ResultData{
    final private String token;
    
    /**
     * Create a new intent of the TokenResultData object
     * @param json the JsonObject containing the data.
     * @throws JSONException 
     */
    public TokenResultData(JSONObject json) throws JSONException {
        super(json);
        
         this.token = json.getString("Token");
    }

    /**
     * Get the token.
     * @return The <b>Token</b> variable.
     */
    public String getToken() {
        return token;
    }
}
