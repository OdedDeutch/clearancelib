/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The InvoiceResult class is holding the data received from Pelecard webservice from the "CreateInvoice" method.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class InvoiceResult extends Result {
    private final TamalData tamalData;

    /**
     * Create a new InvoiceResult object based on a Json object received from Pelecard.
     * @param json
     * @throws JSONException 
     */
    public InvoiceResult(JSONObject json) throws JSONException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        tamalData = new TamalData(json);
    }

    /**
     * Get the TamalData object
     * @return TamalData object 
     */
    public TamalData getTamalData() {
        return tamalData;
    }
}
