/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import java.text.ParseException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class BroadcasrResult extends Result{

    final private BroadcastResultData resultData;
    
    public BroadcasrResult(JSONObject json) throws JSONException, ParseException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData"))
            resultData = new BroadcastResultData(json.getJSONObject("ResultData"));
        else
            resultData = null;
    }

    public BroadcastResultData getResultData() {
        return resultData;
    }
}
