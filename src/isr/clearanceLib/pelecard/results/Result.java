/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

/**
 * The Results object represent a result from a request to Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public abstract class Result {
    final private String statusCode;
    final private String statusMessage;
    
    public Result(String statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    /**
     * Get the status code of the request.
     * @return The <b>StatusCode</b> variable.
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Get the status message of the request.
     * @return The <b>StatusMessage</b> variable.
     */
    public String getStatusMessage() {
        return statusMessage;
    }
}
