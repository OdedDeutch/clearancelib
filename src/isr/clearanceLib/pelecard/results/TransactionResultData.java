/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The TransactionResultData is in use by the Debit, Authorize and ClearPending methods in the Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TransactionResultData extends ResultData{
    private final String pelecardTransactionId;
    
    /**
     * Create a new intent of the DebitResultData object
     * @param json the JsonObject containing the data.
     * @throws JSONException 
     */
    public TransactionResultData(JSONObject json) throws JSONException {
        super(json);
        
        this.pelecardTransactionId = json.getString("PelecardTransactionId");
    }

    /**
     * Get the Pelecard transaction id.
     * @return The <b>PelecardTransactionId</b> variable.
     */
    public String getPelecardTransactionId() {
        return pelecardTransactionId;
    }
    
    
}
