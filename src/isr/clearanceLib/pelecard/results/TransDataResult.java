/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TransDataResult extends Result { 
    final private TransactionResultData[] resultData;
    
     /**
     * Create a new TransDataResult object based on a Json object received from Pelecard.
     * @param json
     * @throws JSONException 
     */
    public TransDataResult(JSONObject json) throws JSONException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData")) {
            JSONArray objectArray = json.getJSONArray("ResultData");
            resultData = new TransactionResultData[objectArray.length()];
            for(int idx = 0; idx < resultData.length; idx++ ){
                resultData[idx] = new TransactionResultData(objectArray.getJSONObject(idx));
            }
        }  else {
            resultData = null;
        }
    }

    /**
     * 
     * @return 
     */
    public TransactionResultData[] getResultData() {
        return resultData;
    }
}
