/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import isr.clearanceLib.pelecard.definitions.Currency;
import isr.clearanceLib.pelecard.definitions.CreditCardBrand;
import isr.clearanceLib.pelecard.definitions.DebitType;
import isr.clearanceLib.pelecard.definitions.CreditType;
import isr.clearanceLib.pelecard.definitions.DebitCode;
import isr.clearanceLib.pelecard.definitions.CreditCompany;
import isr.clearanceLib.pelecard.definitions.SapakimMutavim;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TrxLookupResultData {
    final private String terminalNumber;
    final private String shovar;
    final private Currency currency;
    final private String dayInMonth;
    final private String expDate;
    final private int amount;
    final private String authorizationNo;
    final private String cardNumber;
    final private String paramX;
    final private String recSource;
    final private String ricuzNumber;
    final private String sapakMutavNo;
    final private int firstAmount;
    final private int notFirstAmount;
    final private int cochavAmount;
    final private String time;
    final private CreditCardBrand brand;
    final private String clubCode;
    final private CreditCompany creditCompany;
    final private CreditType creditTerms;
    final private String fileNumber;
    final private int numberOfPayments;
    final private SapakimMutavim sapakimMutavim;
    final private String termNo;
    final private String termSeq;
    final private DebitCode tranCode;
    final private DebitType tranType;
    final private Date broadcastDate;        

    public TrxLookupResultData(JSONObject json) throws Exception {
        this.terminalNumber = json.getString("TerminalNumber");
        this.shovar = json.getString("Shovar");
        this.currency = Currency.convert(json.getInt("Currency"));
        this.dayInMonth = json.getString("DayInMonth");
        this.expDate = json.getString("ExpDate");
        this.amount = json.getInt("Amount");
        this.authorizationNo = json.getString("AuthorizationNo");
        this.cardNumber = json.getString("CardNumber");
        this.paramX = json.getString("ParamX");
        this.recSource = json.getString("RecSource");
        this.ricuzNumber = json.getString("RicuzNumber");
        this.sapakMutavNo = json.getString("SapakMutavNo");
        this.firstAmount = json.getInt("FirstAmount");
        this.notFirstAmount = json.getInt("NotFirstAmount");
        this.cochavAmount = json.getInt("CochavAmount");
        this.time = json.getString("Time");
        this.brand = CreditCardBrand.convert(json.getInt("Brand"));
        this.clubCode = json.getString("ClubCode");
        this.creditCompany = CreditCompany.convert(json.getInt("CreditCompany"));
        this.creditTerms = CreditType.convert(json.getInt("CreditTerms"));
        this.fileNumber = json.getString("FileNumber");
        this.numberOfPayments = json.getInt("NUmberOfPayments");
        this.sapakimMutavim = SapakimMutavim.convert(json.getInt("SapakimMutavim"));
        this.termNo = json.getString("TermNo");
        this.termSeq = json.getString("TermSeq");
        this.tranCode = DebitCode.convert(json.getInt("TranCode"));
        this.tranType = DebitType.convert(json.getInt("tranType"));
        DateFormat format = new SimpleDateFormat("dd/MM/YYYY HH:mm");
        this.broadcastDate = format.parse(json.getString("BroadcastDate"));
    }

    public String getTerminalNumber() {
        return terminalNumber;
    }

    public String getShovar() {
        return shovar;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getDayInMonth() {
        return dayInMonth;
    }

    public String getExpDate() {
        return expDate;
    }

    public int getAmount() {
        return amount;
    }

    public String getAuthorizationNo() {
        return authorizationNo;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getParamX() {
        return paramX;
    }

    public String getRecSource() {
        return recSource;
    }

    public String getRicuzNumber() {
        return ricuzNumber;
    }

    public String getSapakMutavNo() {
        return sapakMutavNo;
    }

    public int getFirstAmount() {
        return firstAmount;
    }

    public int getNotFirstAmount() {
        return notFirstAmount;
    }

    public int getCochavAmount() {
        return cochavAmount;
    }

    public String getTime() {
        return time;
    }

    public CreditCardBrand getBrand() {
        return brand;
    }

    public String getClubCode() {
        return clubCode;
    }

    public CreditCompany getCreditCompany() {
        return creditCompany;
    }

    public CreditType getCreditTerms() {
        return creditTerms;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public int getNumberOfPayments() {
        return numberOfPayments;
    }

    public SapakimMutavim getSapakimMutavim() {
        return sapakimMutavim;
    }

    public String getTermNo() {
        return termNo;
    }

    public String getTermSeq() {
        return termSeq;
    }

    public DebitCode getTranCode() {
        return tranCode;
    }

    public DebitType getTranType() {
        return tranType;
    }

    public Date getBroadcastDate() {
        return broadcastDate;
    }
    
    
    
}
