/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The TransactionResult class is holding the data received from Pelecard webservice from the "Debit", 
 * "Authorize" and "ClearPending" methods.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TransactionResult extends Result{
    final private TransactionResultData resultData;
    final private TamalData tamalData;
    
    public TransactionResult(JSONObject json) throws JSONException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData"))
            resultData = new TransactionResultData(json.getJSONObject("ResultData"));
        else
            resultData = null;
        
        if(json.has("TamalData"))
            tamalData = new TamalData(json.getJSONObject("TamalData"));
        else
            tamalData = null;
    }

    /**
     * Get the ResultData object
     * @return ResultData object 
     */
    public TransactionResultData getResultData() {
        return resultData;
    }

    /**
     * Get the TamalData object, if exist.
     * If not, return null.
     * @return the TamalData object.
     */
    public TamalData getTamalData() {
        return tamalData;
    }
}
