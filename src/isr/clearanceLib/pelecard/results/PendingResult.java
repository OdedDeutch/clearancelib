/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The TransactionResult class is holding the data received from Pelecard webservice from the "Pending"
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class PendingResult extends Result {

    final private PendingResultData resultData;
    
    /**
     * Create a new intent of PendingResult based on the Json received from Pelecard API.
     * @param json the Json object containing the data.
     * @throws JSONException 
     */
    public PendingResult(JSONObject json) throws JSONException {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData"))
            this.resultData = new PendingResultData(json.getJSONObject("ResultData"));
        else
            resultData = null;
    }
    
    /**
     * Get the ResultData object
     * @return ResultData object 
     */
    public PendingResultData getResultData() {
        return resultData;
    }
}
