/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class BroadcastResultData {
    final private Date broadcastDate;
    final private String fileNumber;
    final private String ricuzNumber;
    final private String terminalNumber;
    
    /**
     * 
     * @param json
     * @throws JSONException
     * @throws ParseException 
     */
    public BroadcastResultData(JSONObject json) throws JSONException, ParseException {
        DateFormat format = new SimpleDateFormat("dd/MM/YYYY HH:mm");
        this.broadcastDate = format.parse(json.getString("BroadcastDate"));
        this.fileNumber = json.getString("FileNumber");
        this.ricuzNumber = json.getString("RicuzNumber");
        this.terminalNumber = json.getString("TerminalNumber");
    }

    /**
     * 
     * @return 
     */
    public Date getBroadcastDate() {
        return broadcastDate;
    }

    /**
     * 
     * @return 
     */
    public String getFileNumber() {
        return fileNumber;
    }

    /**
     * 
     * @return 
     */
    public String getRicuzNumber() {
        return ricuzNumber;
    }

    /**
     * 
     * @return 
     */
    public String getTerminalNumber() {
        return terminalNumber;
    }
    
    
}
