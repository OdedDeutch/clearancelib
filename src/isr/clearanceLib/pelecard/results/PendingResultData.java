/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import isr.clearanceLib.pelecard.results.ResultData;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The DebitResultData is in use by the Pending methods in the Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class PendingResultData extends ResultData {
    private final String pendingRecordId;
     
    public PendingResultData(JSONObject json) throws JSONException {
        super(json);
        
        this.pendingRecordId = json.getString("PendingRecordId");
    }

    /**
     * Get the pending record id.
     * @return The <b>PendingRecordId</b> variable.
     */
    public String getPendingRecordId() {
        return pendingRecordId;
    }
}
