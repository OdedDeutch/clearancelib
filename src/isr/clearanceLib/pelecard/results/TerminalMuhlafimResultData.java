/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import isr.clearanceLib.pelecard.definitions.Source;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TerminalMuhlafimResultData {
    final private String supplierNumber;
    final private Date creationDate;
    final private Source source;
    final private String fileName;
    final private String actionDescription;
    final private String token;
    final private String cardNumber;
    final private String newCardNumber;
    final private String newExpirationDate;
    
    public TerminalMuhlafimResultData(JSONObject json) throws JSONException, ParseException {
        this.supplierNumber = json.getString("SupplierNumber");
        DateFormat format = new SimpleDateFormat("dd/MM/YYYY HH:mm");
        this.creationDate = format.parse(json.getString("CreationDate"));
        this.source = Source.convert(json.getInt("source"));
        this.fileName = json.getString("FileName");
        this.actionDescription = json.getString("ActionDescription");
        this.token = json.getString("Token");
        this.cardNumber = json.getString("CardNumber");
        this.newCardNumber = json.getString("NewCardNumber");
        this.newExpirationDate = json.getString("NewExpirationDate");
    }

    public String getSupplierNumber() {
        return supplierNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Source getSource() {
        return source;
    }

    public String getFileName() {
        return fileName;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public String getToken() {
        return token;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getNewCardNumber() {
        return newCardNumber;
    }

    public String getNewExpirationDate() {
        return newExpirationDate;
    }
    
    
}
