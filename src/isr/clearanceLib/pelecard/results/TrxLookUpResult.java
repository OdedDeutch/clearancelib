/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.results;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TrxLookUpResult extends Result{
    final private TrxLookupResultData[] resultData;
    
    public TrxLookUpResult(JSONObject json) throws Exception {
        super(json.getString("StatusCode"), json.getString("ErrorMessage"));
        
        if(json.has("ResultData")) {
            JSONArray objectArray = json.getJSONArray("ResultData");
            resultData = new TrxLookupResultData[objectArray.length()];
            for(int idx = 0; idx < resultData.length; idx++ ){
                resultData[idx] = new TrxLookupResultData(objectArray.getJSONObject(idx));
            }
        } else {
            resultData = null;
        }
    }

    public TrxLookupResultData[] getResultData() {
        return resultData;
    }
    
}
