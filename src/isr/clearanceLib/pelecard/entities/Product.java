/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.entities;

/**
 * 
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class Product {
    final private String name;
    final private int quantity;
    final private int amount;

    public Product(String name, int quantity, int amount) {
        this.name = name;
        this.quantity = quantity;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return name + "," + quantity + "," + amount;
    }
}
