/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.entities;

/**
 * The CreditCard object is used to store a credit card details.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class CreditCard {
    final private String number;
    final private String cvv2;
    final private String validationDate;
    final private String id;
    final private String token;

    public CreditCard(String number) {
        this(number, "", "", "");
    }
    
    /**
     * Create a new CreditCard instant.
     * @param number - The credit card number.
     * @param validationDate - Card's validity date, in the format of MMYY.
     */
    public CreditCard(String number, String validationDate) {
        this(number, validationDate, "", "");
    }
    
    /**
     * Create a new CreditCard instant.
     * @param number - The credit card number.
     * @param validationDate - Card's validity date, in the format of MMYY.
     * @param cvv2 - The CVV2 code.
     * @param id - Client id number.
     */
    public CreditCard(String number, String validationDate, String cvv2, String id) {
       
        this.number = number;
        this.validationDate = validationDate;
        this.cvv2 = cvv2;
        this.id = id;
        this.token = "";
    }
    
    /**
     * Get the token value.
     * @return the <b>token</b> variable.
     */
    public String getToken() {
        return token;
    }
    
    /**
     * Get the client id number;
     * @return the <b>id</b> variable.
     */
    public String getId() {
        return id;
    }
    
    /**
     * Get the number of the card.
     * @return the <b>number</b> variable.
     */
    public String getNumber() {
        return number;
    }

    /** 
     * Get the CVV2 number of the card.
     * @return cvv2
     */
    public String getCvv2() {
        return cvv2;
    }

    /**
     * Get the validity date of the number
     * @return validationDate
     */
    public String getValidationDate() {
        return validationDate;
    }
}
