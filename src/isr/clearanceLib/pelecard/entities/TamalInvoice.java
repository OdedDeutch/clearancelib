package isr.clearanceLib.pelecard.entities;

import isr.clearanceLib.pelecard.entities.Product;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * 
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TamalInvoice {
    final private String invoiceUserName;
    final private String invoicePassword;
    final private String esekNum;
    final private String typeCode;
    final private String printLanguage;
    final private String clientNumber;
    final private String clientName;
    final private String clientAddress;
    final private String clientCity;
    final private String clientEmail;
    final private int nikuyBamakorSum;
    final private int maamRate;
    final private String docDetails;
    final private Product productsListFromWeb;
    final private String toSign;

    public TamalInvoice(String invoiceUserName, String invoicePassword, String esekNum, 
            String typeCode, String printLanguage, String clientNumber, String clientName, 
            String clientAddress, String clientCity, String clientEmail, int nikuyBamakorSum, 
            int maamRate, String docDetails, Product productsListFromWeb, String toSign) {
        this.invoiceUserName = invoiceUserName;
        this.invoicePassword = invoicePassword;
        this.esekNum = esekNum;
        this.typeCode = typeCode;
        this.printLanguage = printLanguage;
        this.clientNumber = clientNumber;
        this.clientName = clientName;
        this.clientAddress = clientAddress;
        this.clientCity = clientCity;
        this.clientEmail = clientEmail;
        this.nikuyBamakorSum = nikuyBamakorSum;
        this.maamRate = maamRate;
        this.docDetails = docDetails;
        this.productsListFromWeb = productsListFromWeb;
        this.toSign = toSign;
    }

    public String getInvoiceUserName() {
        return invoiceUserName;
    }

    public String getInvoicePassword() {
        return invoicePassword;
    }

    public String getEsekNum() {
        return esekNum;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public String getPrintLanguage() {
        return printLanguage;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public String getClientCity() {
        return clientCity;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public int getNikuyBamakorSum() {
        return nikuyBamakorSum;
    }

    public int getMaamRate() {
        return maamRate;
    }

    public String getDocDetails() {
        return docDetails;
    }

    public Product getProductsListFromWeb() {
        return productsListFromWeb;
    }

    public String getToSign() {
        return toSign;
    }
    
    public JSONObject toJson() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("InvoiceUserName", invoiceUserName);
        json.put("InvoicePassword", invoicePassword);
        json.put("EsekNum", esekNum);
        json.put("TypeCode", typeCode);
        json.put("PrintLanguage", printLanguage);
        json.put("ClientNumber", clientNumber);
        json.put("ClientName", clientName);
        json.put("ClientAddress", clientAddress);
        json.put("ClientCity", clientCity);
        json.put("EmaiAddress", clientEmail);
        json.put("NikuyBamakorSum", nikuyBamakorSum);
        json.put("MaamRate", maamRate);
        json.put("DocDetails", docDetails);
        json.put("ProductsListFromWeb", productsListFromWeb);
        json.put("ToSign", toSign);
        return json;
    }
}
