/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.entities;

/**
 * PaymentType is used to point the type of the payment. 
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum PaymentType {
    Regular, Payments, Credit, Isracredit
}
