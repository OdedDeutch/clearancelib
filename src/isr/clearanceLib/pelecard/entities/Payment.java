/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.entities;

import isr.clearanceLib.pelecard.definitions.Currency;
import java.security.InvalidParameterException;

/**
 * The Payment class hold the details of the payment it self, total sum, currency and other payments details.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class Payment {
    final private int total;
    final private Currency currency;
    final private int paymentsNumber;
    final private int firstPayment;

    /**
     * Create a new intent of Payment object. to hold the payment details for the Pelecard API.
     * @param total The <b>total</b> variable in agorot.
     * @param currency The <b>currency</b> variable.
     * @param paymentNumber The <b>paymentNumber</b> variable.
     * @param firstPayment The <b>firstPayment</b> variable agorot.
     */
    public Payment(int total, Currency currency, int paymentNumber, int firstPayment) {
        if(paymentNumber <1)
            throw new InvalidParameterException("The paymentNumeber parameter can not be smaller then 1.");
        
         if(firstPayment <1)
            throw new InvalidParameterException("The firstPayment parameter can not be smaller then 1.");
        
        this.total = total;
        this.currency = currency;
        this.paymentsNumber = paymentNumber;
        this.firstPayment = firstPayment;
    }

    /**
     * Create a new intent of Payment object. to hold the payment details for the Pelecard API.
     * the <b>firstPayment</b> variable will be equal to <b>total/paymentNumeber</b>.
     * @param total The <b>total</b> variable in agorot.
     * @param currency The <b>currency</b> variable.
     * @param paymentsNumber The <b>paymentsNumber</b> variable. 
     */
    public Payment(int total, Currency currency, int paymentsNumber) {  
        if(paymentsNumber <1)
            throw new InvalidParameterException("The paymentNumeber parameter can not be smaller then 1.");
        
        this.total = total;
        this.currency = currency;
        this.paymentsNumber = paymentsNumber;
        this.firstPayment = total/paymentsNumber;
    }
    
    /**
     * Create a new intent of Payment object. to hold the payment details for the Pelecard API.
     * <b>paymentNumber</b> will be equals to 1 and  <b>firstPayment</b> will be equal to the <b>total</b> variable
     * @param total The <b>total</b> variable.
     * @param currency The <b>currency</b> variable.
     */
    public Payment(int total, Currency currency) {
        this.total = total;
        this.currency = currency;
        this.paymentsNumber = 1;
        this.firstPayment = total;
    }

    /**
     * Get the total sum of the deal in agorot.
     * @return The <b>total</b> variable
     */
    public int getTotal() {
        return total;
    }

    /**
     * Get the deal currency 
     * @return The <b>currency</b> variable
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Get the number of the payments.
     * @return The <b>paymentsNumber</b> variable
     */
    public int getPaymentsNumber() {
        return paymentsNumber;
    }
    /**
     * Get the first payment sum in agorot
     * @return The <b>firstPayment</b> variable
     */
    public int getFirstPayment() {
        return firstPayment;
    }
}
