/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The TransCode enum define the deal structure according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum TransCode {
    NormalDeal(0),
    SpecialCredit(1),
    Payments(8),
    VehicleFleetFuelDeal(9),
    Other(99); 
    
    private final int type;
    
    private TransCode(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static TransCode convert(int value) {
        switch(value) {
            case 0:
                return TransCode.NormalDeal;
                            
            case 1:
                return TransCode.SpecialCredit;
                
            case 8:
                return TransCode.Payments;
                
            case 9:
                return TransCode.VehicleFleetFuelDeal;
                
            default:
                return TransCode.Other;
        }
    }
}
