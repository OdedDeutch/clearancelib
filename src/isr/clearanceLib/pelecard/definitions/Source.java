/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The Source enum define the source of the exchanged file according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum Source {
    Isracart(1), 
    VisaCal(2),
    LeumiCard(6),
    Other(99);
    
    private final int type;
    
    private Source(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static Source convert(int value) {
        switch(value) {     
            case 1:
                return Source.Isracart;
                
            case 2:
                return Source.VisaCal;
                
            case 6:
                return Source.LeumiCard;

            default:
               return Source.Other;
        }
    }
}
