/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The Currency enum define the currencies according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum Currency {
    Special(0),
    NIS(1),
    USD(2),
    PaymentInUSD(4),
    PaymentsInNIS(8),
    Other(99);
    
    private final int currency;
    
    private Currency(int curr) {
        this.currency = curr;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return currency;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static Currency convert(int value) {
        switch(value) {
            case 0:
                return Currency.Special;
                            
            case 1:
                return Currency.NIS;
                
            case 2:
                return Currency.USD;
                
            case 3:
                return Currency.PaymentInUSD;
                
            case 4:
                return Currency.PaymentsInNIS;
                
            default:
                return Currency.Other;
        }
    }
}
