/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The ApprovedBy enum define the identity deal approver according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum ApprovedBy {
    Shava(1),
    CreditCardCompany(2),
    VoiceResponseMachine(3),
    NotApproved(0),
    Other(99); 
    
    private final int type;
    
    private ApprovedBy(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static ApprovedBy convert(int value) {
        switch(value) {
            case 0:
                return ApprovedBy.NotApproved;
             
            case 1:
                return ApprovedBy.Shava;
                
            case 2:
                return ApprovedBy.CreditCardCompany;
                
            case 3:
                return ApprovedBy.VoiceResponseMachine;

            default:
                return ApprovedBy.Other;
        }
    }
}
