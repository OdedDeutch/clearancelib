/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The DebitCode enum define the credit types according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum DebitCode {
    NormalDeal(0),
    SelfService(1),
    SelfServiceGasStation(2),
    Contactless(5),
    ContactlessSelfService(6),
    PhoneDeal(50),
    SignDealOnly(51),
    Other(99);
    
    final private int type;
    
    private DebitCode(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static DebitCode convert(int value) {
        switch(value) {
            case 0:
                return DebitCode.NormalDeal;
             
            case 1:
                return DebitCode.SelfService;
                
            case 2:
                return DebitCode.SelfServiceGasStation;
                
            case 5:
                return DebitCode.Contactless;
                
            case 6:
                return DebitCode.ContactlessSelfService;
                
            case 50:
                return DebitCode.PhoneDeal;
                
            case 51:
                return DebitCode.SignDealOnly;
                
            default:
                return DebitCode.Other;
        }
    }
}
