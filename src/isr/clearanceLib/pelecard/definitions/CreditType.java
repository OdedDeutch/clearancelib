/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The CreditType enum define the credit types according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum CreditType {
    Regular(1),
    Credit(2),
    InstentDebit(3),
    ClubCredit(4),
    SuperCredit(5),
    CreditInPayments(6),
    Payments(8),
    ClubPayment(9),
    Other(99);
    
    private final int type;
    
    private CreditType(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static CreditType convert(int value) {
        switch(value) {
            case 1:
                return CreditType.Regular;
                
            case 2:
                return CreditType.Credit;
                
            case 3:
                return CreditType.InstentDebit;
                
            case 4:
                return CreditType.ClubCredit;
                
            case 5:
                return CreditType.SuperCredit;
             
            case 6:
                return CreditType.CreditInPayments;
             
            case 8:
                return CreditType.Payments;
             
            case 9:
                return CreditType.ClubPayment;
             
            default:
                return CreditType.Other;
        }
    }
}
