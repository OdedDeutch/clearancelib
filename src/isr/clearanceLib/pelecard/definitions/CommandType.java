/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The CommandType enum is defining the command type of the Pelecard API.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum CommandType {
    DebitRegularType,
    DebitCreditType,
    DebitPaymentsType,
    DebitIsracreditType,
    AuthorizeCreditCard,
    AuthorizeCreditType,
    AuthorizePaymentsType,
    AuthorizeIsracreditCard,
    PendingRegularType,
    PendingCreditType,
    PendingPaymentsType,
    PendingIsracreditType,
    ClearPendingByUserKey,
    ClearPendingByRecordId,
    ConvertToToken,
    CreateInvoice,
    TrxLookUp,
    ChkGoodParmX,
    GetTransData,
    GetBroadCast,
    GetTerminalMuhlafim,
    DebitbyIntIn,
    GetErrorMessageHe,
    GetErrorMessageEn
}
