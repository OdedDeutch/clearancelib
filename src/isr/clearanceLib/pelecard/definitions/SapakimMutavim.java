/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The SapakimMutavim enum define the providers/Beneficiaries according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum SapakimMutavim {
    NormalDeal(0),
    MultiProvider(1),
    MultiBeneficiar(2),
    Other(99); 
    
    private final int type;
    
    private SapakimMutavim(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static SapakimMutavim convert(int value) {
        switch(value) {
            case 0:
                return SapakimMutavim.NormalDeal;
                            
            case 1:
                return SapakimMutavim.MultiProvider;
                
            case 2:
                return SapakimMutavim.MultiBeneficiar;
                
            default:
                return SapakimMutavim.Other;
        }
    }
}
