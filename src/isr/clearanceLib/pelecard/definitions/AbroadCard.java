/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The AbroadCard enum define the card definition according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum AbroadCard {
    Israeli(0),
    Torist(1), 
    GasStation(2),
    InstantDebit(3),
    GiftCard(4),
    Other(99);
    
    final private int type;
    
    private AbroadCard(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static AbroadCard convert(int value) {
        switch(value) {
            case 0:
                return AbroadCard.Israeli;
             
            case 1:
                return AbroadCard.Torist;
                
            case 2:
                return AbroadCard.GasStation;
                
            case 3:
                return AbroadCard.InstantDebit;
                
            case 4:
                return AbroadCard.GiftCard;
               
            default:
                return AbroadCard.Other;
        }
    }
}
