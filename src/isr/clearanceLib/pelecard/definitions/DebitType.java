/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The DebitType enum define the credit types according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum DebitType {
    Blocked(0),
    NormalDebitDeal(1),
    ApprovedDebitDeal(2),
    ForcedDeal(3),
    CreditDeal(51),
    CancelationDeal(52),
    ApprovedCreditDeal(53),
    Other(99);
    
    final private int type;
    
    private DebitType(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
     /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */   
    public static DebitType convert(int value) {
        switch(value) {
            case 0:
                return DebitType.Blocked;
             
            case 1:
                return DebitType.NormalDebitDeal;
                
            case 2:
                return DebitType.ApprovedDebitDeal;
                
            case 3:
                return DebitType.ForcedDeal;
                
            case 51:
                return DebitType.CreditDeal;
                
            case 52:
                return DebitType.CancelationDeal;
                
            case 53:
                return DebitType.ApprovedCreditDeal;
                
            default:
                return DebitType.Other;
        }
    }

}
