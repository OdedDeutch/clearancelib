/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;


/**
 * The CallReason enum define the call reason code according to Pelecard documentation.
 * Note that the code will arrive in a string format of the hex value (A=10) while an empty string is equals to 0.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum CallReason {
    NoCommunication(""),
    Random("1"),
    Limit("2"),
    SecrateCode("3"),
    ServiceCodeInMagneticStipe("4"),
    ApproveRequestWithoutDeal("5"),
    Blocked("6"),
    ZFL("7"),
    ApproveRequest("8"),
    Loading("9"),
    Unloading("10"),
    Other("99");
    
    final private String type;
    
    private CallReason(String type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return A String with the value.
     */
    public String getValue() {
        return type;
    }
    
    /**
     * Convert a String to an enum object.
     * @param value The String to be converted.
     * @return a DebitCode object.
     */
    public static CallReason convert(String value) {
        switch(value) {
            case "":
                return CallReason.NoCommunication;
             
            case "1":
                return CallReason.Random;
                
            case "2":
                return CallReason.Limit;
                
            case "3":
                return CallReason.SecrateCode;

            case "4":
                return CallReason.ServiceCodeInMagneticStipe;
               
            case "5":
                return CallReason.ApproveRequestWithoutDeal;
                
            case "6":
                return CallReason.Blocked;
                
            case "7":
                return CallReason.ZFL;
                
            case "8":
                return CallReason.ApproveRequest;
                
            case "9":
                return CallReason.Loading;
                
            case "A":
                return CallReason.Unloading;
   
            default:
                return CallReason.Other;
        }
    }
}
