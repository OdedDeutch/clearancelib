/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;


/**
 * The CreditCompay enum define the credit card companies according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum CreditCompany {
    Undefined(0),
    Isracart(1),
    VisaCal(2),
    Diners(3),
    AmericanExpress(4),
    JBC(5), 
    LeumiCard(6),
    Other(99);
    
    private final int company;
    
    private CreditCompany(int company) {
        this.company = company;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return company;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static CreditCompany convert(int value) {
        switch(value) {
            case 0:
                return CreditCompany.Undefined;
            case 1:
                return CreditCompany.Isracart;
             
            case 2:
                return CreditCompany.VisaCal;
                
            case 3:
                return CreditCompany.Diners;
                
            case 4:
                return CreditCompany.AmericanExpress;
                
            case 5:
                return CreditCompany.JBC;
                
            case 6:
                return CreditCompany.LeumiCard;
                
            default:
                return CreditCompany.Other;
        }
    }
}
