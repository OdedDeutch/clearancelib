/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.definitions;

/**
 * The CreditCardBrand enum define the credit card brand according to Pelecard documentation.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum CreditCardBrand {
    PrivateBrand(0),
    Mastercard(1),
    Visa(2), 
    Mastro(3),
    Isracard(4),
    Other(99);
    
    private final int type;
    
    private CreditCardBrand(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    /**
     * Convert an integer to an enum object.
     * @param value The integer to be converted.
     * @return a DebitCode object.
     */
    public static CreditCardBrand convert(int value) {
        switch(value) {
            case 0:
                return CreditCardBrand.PrivateBrand;
             
            case 1:
                return CreditCardBrand.Mastercard;
                
            case 2:
                return CreditCardBrand.Visa;
                
            case 3:
                return CreditCardBrand.Mastro;
                
            case 4:
                return CreditCardBrand.Isracard;
 
            default:
                return CreditCardBrand.Other;
        }
    }
}
