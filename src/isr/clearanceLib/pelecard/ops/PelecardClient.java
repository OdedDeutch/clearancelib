/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.ops;

import isr.clearanceLib.pelecard.entities.Payment;
import isr.clearanceLib.pelecard.entities.CreditCard;
import isr.clearanceLib.pelecard.entities.PaymentType;
import isr.clearanceLib.pelecard.entities.TamalInvoice;
import isr.clearanceLib.pelecard.results.*;
import isr.clearanceLib.common.TerminalInfo;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;
import javax.net.ssl.HttpsURLConnection;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import isr.commonTools.ILogger;
/**
 * PelecardClient is the gateway to the Pelecard webservice.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class PelecardClient implements IPelecardClient {
    final private TerminalInfo terminal;
    private ILogger logger = new dummyLogger();
    private JSONObject requestJson;
    private JSONObject respondJson;
    private String cmdUrl;
    
    public PelecardClient(TerminalInfo terminalInfo) {
        this.terminal = terminalInfo;
    }
    
    public PelecardClient(String name, String terminalNumber, String user, String password, String url) {
        this.terminal = new TerminalInfo(name, terminalNumber, user, password, url);
    }

    @Override
    public JSONObject getRequestJson() {
        return requestJson;
    }

    @Override
    public JSONObject getRespondJson() {
        return respondJson;
    }

    @Override
    public String getCmdUrl() {
        return cmdUrl;
    }
    
    @Override
    public void setLogger(ILogger logger) {
        this.logger = logger;
    }
    
    private String post(String operationName, JSONObject json) throws Exception{
        URL url = new URL(terminal.getUrl() + "/" + operationName);
        cmdUrl = url.getPath();
        requestJson = json;
        
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("content-type", "application/json");
        con.setRequestProperty("Accept","application/json");
        
        con.setDoOutput(true);
        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.writeBytes(json.toString());
            wr.flush();
        }
        StringBuilder response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        logger.WriteLn("IN: " + response.toString());
        return response.toString();
    }
        
    private JSONObject sendPost(String operationName, JSONObject json) throws Exception {
        logger.WriteLn("OUT: " + json.toString());
        JSONObject obj = new JSONObject(post(operationName, json));  
        respondJson = obj;
        return obj;
    }
  
    private JSONObject buildJsonObject(String shopNumber, CreditCard creditCard, 
            Payment payment, String paramX) throws JSONException {
        JSONObject json = buildClearJson();
        json.put("shopNumber", shopNumber);
        json.put("creditCard", creditCard.getNumber());
        json.put("creditCardDateMmYy", creditCard.getValidationDate());
        json.put("token", creditCard.getToken());
        json.put("total", payment.getTotal());
        json.put("currency", payment.getCurrency().getValue());
        json.put("cvv2", creditCard.getCvv2());
        json.put("id", creditCard.getId());
        json.put("paramX", paramX);
        return json;
    }
    
    private JSONObject buildClearJson() throws JSONException{
        JSONObject json = new JSONObject();
        json.put("terminalNumber", terminal.getTerminalNumber());
        json.put("user", terminal.getUserName());
        json.put("password", terminal.getPassword());
        return json;
    }
    
    /**
     * Preform a debit with the Pelecard WS.
     * @param type the type of the payment.
     * @param shopNumber The identity of the shop.
     * @param creditCard A CreditCard Object containing the credit card details.
     * @param payment A Payment object containing the payment details.
     * @param authorizationNumber An authorization number.
     * @param paramX Free text, up to 19 chars.
     * @param tamalInvoice invoice details should one need to issue with Tamal service - Optional, can be null.
     * @return TransactionResult containing the result data.
     * @throws isr.clearanceLib.pelecard.ops.PelecardClientException
     * @throws Exception
     */
    @Override
    public TransactionResult DebitCard(PaymentType type, String shopNumber, 
            CreditCard creditCard, Payment payment, String authorizationNumber, 
            String paramX, TamalInvoice tamalInvoice) throws PelecardClientException, Exception{
        
        JSONObject json = buildJsonObject(shopNumber, creditCard, payment, paramX);
        String cmd = null;
        switch (type) {
            case Regular:
                cmd = "DebitRegularType";
                json.put("authorizationNumber", authorizationNumber);
                json.put("paymentsNumber", payment.getPaymentsNumber());
                break;
                
            case Payments:
                cmd = "DebitPaymentsType";
                json.put("authorizationNumber", authorizationNumber);
                json.put("paymentsNumber", payment.getPaymentsNumber());
                json.put("firstPayment", payment.getFirstPayment());
                break;
                
            case Credit:
                cmd = "DebitCreditType";
                json.put("authorizationNumber", authorizationNumber);
                json.put("paymentsNumber", payment.getPaymentsNumber());
                break;
                
            case Isracredit:
                cmd = "DebitIsracreditType";
                json.put("authorizationNumber", authorizationNumber);
                break;
        }
        
        if(tamalInvoice != null) {
            json.put("TamalInvoice", tamalInvoice.toJson());
        }

        JSONObject pcJson = sendPost(cmd, json);
        try {
            return new TransactionResult(pcJson);
        } catch (Exception ex) {
            throw new PelecardClientException("Error in DebitCard().", cmd, json, pcJson, ex);
        }
    }
    
    /**
     * Authorize a credit card with the Pelecard WS. 
     * @param type the type of the payment.
     * @param shopNumber The identity of the shop.
     * @param creditCard A CreditCard Object containing the credit card details.
     * @param payment A Payment object containing the payment details.
     * @param paramX Free text, up to 19 chars.
     * @return TransactionResult containing the result data.
     * @throws Exception
     */
    @Override
    public TransactionResult AuthorizeCard(PaymentType type, String shopNumber, 
            CreditCard creditCard, Payment payment, String paramX) throws Exception {
        JSONObject json = buildJsonObject(shopNumber, creditCard, payment, paramX);
        String cmd = null;
        switch (type) {
            case Regular:
                cmd = "AuthorizeCreditCard";
                break;
                
            case Payments:
                cmd = "AuthorizePaymentsType";
                json.put("paymentsNumber", payment.getPaymentsNumber());
                json.put("firstPayment", payment.getFirstPayment());
                break;
                
            case Credit:
                cmd = "AuthorizeCreditType";
                break;
                
            case Isracredit:
                cmd = "AuthorizeIsracreditCard";
                break;
        }

        return new TransactionResult(sendPost(cmd, json));
    }
    
    /**
     * Add a pending deal to a credit card with the Pelecard WS.
     * @param type the type of the payment.
     * @param shopNumber The identity of the shop.
     * @param creditCard A CreditCard Object containing the credit card details.
     * @param payment A Payment object containing the payment details.
     * @param authorizationNumber An authorization number.
     * @param paramX Free text, up to 19 chars.
     * @param userKey a key used to identify a users deal.
     * @return
     * @throws Exception
     */
    @Override
    public PendingResult PendCard(PaymentType type, String shopNumber, 
            CreditCard creditCard, Payment payment, String authorizationNumber, 
            String paramX, String userKey) throws Exception {
                JSONObject json = buildJsonObject(shopNumber, creditCard, payment, paramX);
        String cmd = null;
        switch (type) {
            case Regular:
                cmd = "PendingRegularType";
                json.put("authorizationNumber", authorizationNumber);
                json.put("userKey", userKey);
                break;
                
            case Payments:
                cmd = "PendingPaymentsType";
                 json.put("paymentsNumber", payment.getPaymentsNumber());
                json.put("firstPayment", payment.getFirstPayment());
                json.put("userKey", userKey);
                break;
                
            case Credit:
                cmd = "PendingCreditType";
                json.put("paymentsNumber", payment.getPaymentsNumber());
                json.put("userKey", userKey);
                break;
                
            case Isracredit:
                cmd = "PendingIsracreditType";
                json.put("authorizationNumber", authorizationNumber);
                json.put("userKey", userKey);
                break;
        }

        return new PendingResult(sendPost(cmd, json));
    }

    /**
     * Debit a pending deal identified by user key.
     * @param userKey the user key that is used to identify the pended deal.
     * @return TransactionResult containing the result data.
     * @throws Exception 
     */
    @Override
    public TransactionResult ClearPendingByUserKey(String userKey) throws Exception {
        JSONObject json = new JSONObject();
        json.put("terminalNumber", terminal.getTerminalNumber());
        json.put("user", terminal.getUserName());
        json.put("password", terminal.getPassword());
        json.put("userKey", userKey);
        return new TransactionResult(sendPost("ClearPendingByUserKey", json));
    }
    
    /**
     * Debit a pending deal identified by a record id.
     * @param pendingRecordId the id that is used to identify the pended deal.
     * @return
     * @throws Exception 
     */
    @Override
    public TransactionResult ClearPendingByRecordId(String pendingRecordId) throws Exception {
        JSONObject json = buildClearJson();
        json.put("pendingRecordId", pendingRecordId);
        return new TransactionResult(sendPost("ClearPendingByRecordId", json));
    }

    /**
     * Convert credit card details to a token the can be stored and be used in future deals.
     * @param shopNumber the shop number.
     * @param creditCard the credit card number
     * @param creditCardDateMmYy the validity date of the card in a format of MMYY.
     * @param addFourDigits should the last four digits be added
     * @return a TokenResult with the Token details.
     * @throws Exception 
     */
    @Override
    public TokenResult ConvertToToken(String shopNumber, String creditCard, 
            int creditCardDateMmYy, boolean addFourDigits) throws Exception{
        JSONObject json = buildClearJson();
        json.put("shopNumber", shopNumber);
        json.put("creditCard", creditCard);
        json.put("creditCardDateMmYy", creditCardDateMmYy);
        json.put("addFourDigits", addFourDigits);
        return new TokenResult(sendPost("ConvertToToken", json));
    }

    /**
     * Create a Tamal invoice based on Pelecard transaction id.
     * @param trxRecordId Pelecard transaction id.
     * @param tamalInvoice Invoice details.
     * @return InvoiceResult with the Invoice data.
     * @throws Exception 
     */
    @Override
    public InvoiceResult CreateInvoice(String trxRecordId, TamalInvoice tamalInvoice) throws Exception {
        JSONObject json = tamalInvoice.toJson();
        json.put("terminalNumber", terminal.getTerminalNumber());
        json.put("user", terminal.getUserName());
        json.put("password", terminal.getPassword());
        json.put("trxRecordId", trxRecordId);
        return new InvoiceResult(sendPost("CreateInvoice", json));
    }

    /**
     * Receive deals details based on paramX in order to confirm transaction commits.
     * @param shopNumber The identity of the shop.
     * @param paramX Free text, up to 19 chars.
     * @return TrxLookUpResult with the deals information.
     * @throws Exception 
     */
    @Override
    public TrxLookUpResult TrxLookUp(String shopNumber, String paramX) throws Exception {
        JSONObject json = buildClearJson();
        json.put("shopNumber", shopNumber);
        json.put("paramX", paramX);
        return new TrxLookUpResult(sendPost("TrxLookUp", json));
    }

    @Override
    public GoodParmXResult ChkGoodParmX(String shopNumber, String paramX, boolean shvaSuccessOnly) throws Exception {
        JSONObject json = buildClearJson();
        json.put("shopNumber", shopNumber);
        json.put("paramX", paramX);
        json.put("shvaSuccessOnly", shvaSuccessOnly);
        return new GoodParmXResult(sendPost("ChkGoodParmX", json));
    }

    @Override
    public TransDataResult GetTransData(String shopNumber, Date startDate, Date endDate) throws Exception {
        JSONObject json = buildClearJson();
        json.put("shopNumber", shopNumber);
        json.put("startDate", startDate);
        json.put("EndDate", endDate);
        return new TransDataResult(sendPost("GetTransData", json)); 
    }

    @Override
    public BroadcasrResult GetBroadcast(int ricuzNumber) throws Exception {
        JSONObject json = buildClearJson();
        json.put("ricuzNumber", ricuzNumber);
        return new BroadcasrResult(sendPost("GetBroadcast", json)); 
    }

    @Override
    public TerminalMuhlafimResult GetTerminalMuhlafim(Date startDate, Date endDate) throws Exception {
        JSONObject json = buildClearJson();
        json.put("StartDate", startDate);
        json.put("EndDate", endDate);
        return new TerminalMuhlafimResult(sendPost("GetTerminalMuhlafim", json)); 
    }

    @Override
    public TransactionResult DebitByIntIn(String intIn, String token) throws Exception {
        JSONObject json = buildClearJson();
        json.put("intIn", intIn);
        json.put("token", token);
        return new TransactionResult(sendPost("DebitByIntIn", json)); 
    }

    @Override
    public String GetErrorMessageHe(String errorCode) throws Exception {
        JSONObject json = new JSONObject();
        json.put("ErrorCode", errorCode);
        return post("GetErrorMessageHe", json);
    }

    @Override
    public String GetErrorMessageEn(String errorCode) throws Exception {
        JSONObject json = new JSONObject();
        json.put("ErrorCode", errorCode);
        return post("GetErrorMessageEn", json);
    }
    
    private class dummyLogger implements ILogger {
        @Override
        public void WriteLn(String msg) {}
        
        @Override
        public void close() { }
    }
}
