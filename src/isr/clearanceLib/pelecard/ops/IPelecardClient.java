/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.ops;

import isr.clearanceLib.pelecard.results.TransactionResult;
import isr.clearanceLib.pelecard.results.TransDataResult;
import isr.clearanceLib.pelecard.results.TrxLookUpResult;
import isr.clearanceLib.pelecard.results.GoodParmXResult;
import isr.clearanceLib.pelecard.results.TokenResult;
import isr.clearanceLib.pelecard.results.PendingResult;
import isr.clearanceLib.pelecard.results.TerminalMuhlafimResult;
import isr.clearanceLib.pelecard.results.BroadcasrResult;
import isr.clearanceLib.pelecard.entities.PaymentType;
import isr.clearanceLib.pelecard.entities.CreditCard;
import isr.clearanceLib.pelecard.entities.Payment;
import isr.clearanceLib.pelecard.entities.TamalInvoice;
import isr.clearanceLib.pelecard.results.InvoiceResult;
import isr.commonTools.ILogger;
import java.util.Date;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public interface IPelecardClient {
    
    void setLogger(ILogger logger);
    
    JSONObject getRequestJson();

    JSONObject getRespondJson();

    String getCmdUrl();
    
    TransactionResult DebitCard(PaymentType type, String shopNumber, CreditCard creditCard, Payment payment, String authorizationNumber, String paramX, TamalInvoice tamalInfo) throws Exception ;
    
    TransactionResult AuthorizeCard(PaymentType type, String shopNumber, CreditCard creditCard, Payment payment, String paramX) throws Exception ;
    
    PendingResult PendCard (PaymentType type, String shopNumber, CreditCard creditCard, Payment payment, String authorizationNumber, String paramX, String userKey) throws Exception ;

    /* J109 */
    TransactionResult ClearPendingByUserKey(String userKey) throws Exception ;
    
    TransactionResult ClearPendingByRecordId(String pendingRecordId) throws Exception ;
    
    TokenResult ConvertToToken(String shopNumber, String creditCard, int creditCardDateMmYy, boolean addFourDigits) throws Exception ;
    
    InvoiceResult CreateInvoice(String trxRecordId, TamalInvoice tamalInvoice) throws Exception ;
    
    TrxLookUpResult TrxLookUp(String shopNumber, String paramX) throws Exception ;
    
    GoodParmXResult ChkGoodParmX(String shopNumber, String paramX, boolean shvaSuccessOnly) throws Exception ;
    
    TransDataResult GetTransData(String shopNumber, Date startDate, Date endDate) throws Exception ;
    
    BroadcasrResult GetBroadcast(int ricuzNumber) throws Exception ;
    
    TerminalMuhlafimResult GetTerminalMuhlafim(Date startDate, Date endDate) throws Exception ;
    
    TransactionResult DebitByIntIn(String intIn, String token) throws Exception ;
    
    String GetErrorMessageHe(String errorCode) throws Exception ;
    
    String GetErrorMessageEn(String errorCode) throws Exception ;
    
}
