/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.ops;

import org.codehaus.jettison.json.JSONObject;

/**
 * PelecardClientException holds the errors that are based on the JSON received from Pelecard WS.
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class PelecardClientException extends Exception {
    private final JSONObject requestJson;
    private final JSONObject respondJson;
    private final String command;
    
    /**
     * Create an new instant of PelecardClientException
     * @param msg The message that will be apart of the Exception
     * @param command
     * @param requestJson
     * @param respondJson
     * @param exception 
     */
    public PelecardClientException(String msg, String command, JSONObject requestJson, JSONObject respondJson, Exception exception) {
        super(msg, exception);
        this.command = command;
        this.requestJson = requestJson;
        this.respondJson = respondJson;
        
    }

    public JSONObject getRequestJson() {
        return requestJson;
    }
    
    public JSONObject getRespondJson() {
        return respondJson;
    }

    public String getCommand() {
        return command;
    }
}
