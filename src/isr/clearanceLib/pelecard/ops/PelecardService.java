/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceLib.pelecard.ops;

import isr.clearanceLib.common.ClearingService;
import isr.clearanceLib.common.ActionType;
import isr.clearanceLib.pelecard.entities.CreditCard;
import isr.clearanceLib.pelecard.entities.Payment;
import isr.clearanceLib.pelecard.entities.PaymentType;
import isr.clearanceLib.common.TerminalInfo;
import isr.clearanceLib.pelecard.definitions.Currency;
import isr.clearanceLib.pelecard.results.TransactionResult;
import isr.clearanceLib.pelecard.results.TransactionResultData;
import isr.commonTools.StringEx;
import isr.commonTools.JSONLogger;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class PelecardService extends ClearingService {
    IPelecardClient client;

    public PelecardService(TerminalInfo info, String vehicleId, String manOrSwipe, String drNum, 
            String cardNum, String expDate, String cvc, String sum, String type, 
            String pNum, String subPayload, String refNum, String token, String id, String paramX, 
            boolean debug, String rootFolder) {
        super(vehicleId, manOrSwipe, drNum, cardNum, expDate, cvc, sum, type, pNum, 
                subPayload, refNum, token, id, paramX, debug, rootFolder);
        
        TerminalInfo tInfo;
        if(drNum.equals("12121212") || drNum.equals("121212120")) {
            tInfo = TerminalInfo.getTesterTerminal();
        } else {
            tInfo = info;
        }
            
        client = new PelecardClient(tInfo);   
    }

    @Override
    public void commitPayment() throws Exception {
        super.commitPayment();
        CreditCard creditCard = new CreditCard(cardNumDec, expDateDec, cvcDec, id);
        int total = Integer.parseInt(sum);
        if(type == ActionType.Refund) 
            total *= -1;
        Payment payment = new Payment(total, Currency.NIS, Integer.parseInt(pNum)
        );
        
        try { 
            if(debug) { 
                try {
                    jlooger = new JSONLogger(rootFolder, "json.log");
                    client.setLogger(jlooger);     
                } catch (IOException ex) {

                }
            }
            TransactionResult result = client.DebitCard(
                type == ActionType.Payments ? PaymentType.Payments : PaymentType.Regular, 
                vehicleId, creditCard, payment, StringEx.EMPTY,
                paramX, null);

            statusCode = result.getStatusCode();
            if(!"000".equals(statusCode) && debug) {
                writeErrorFile(); 
            }
            TransactionResultData resultData = result.getResultData();
            if(resultData != null) {
                authNumebr = resultData.getPelecardTransactionId();
                String cc = resultData.getCreditCardNumber();
                if(cc.length() > 4)
                    cardLastDigits = cc.substring(cc.length()-4);
                else
                    cardLastDigits = StringEx.EMPTY;
                cardBrand = Integer.toString(resultData.getCreditCardBrand().getValue());
            } else {
                authNumebr = StringEx.EMPTY;
                cardLastDigits = StringEx.EMPTY;
                cardBrand = "0";
            }
        } catch (PelecardClientException ex) {
            if(debug)
                writeErrorFile();
            throw new Exception(ex.getMessage() + " Request: " + ex.getRequestJson() + "Respond: " + ex.getRespondJson(), ex.getCause());
        } finally {
            if(debug && jlooger != null) { 
                jlooger.close(); 
            }
        }
    }    
    
    private void writeErrorFile() {
        JSONLogger logger = null;
        try {
            logger = new JSONLogger(rootFolder + "/Errors", paramX + ".txt");
            logger.WriteLn("Request details:");
            logger.WriteLn("****************");
            logger.WriteLn("Vehicle: \t\t\t" + vehicleId);
            logger.WriteLn("manOrSwipe: \t\t" + manOrSwipe);
            logger.WriteLn("drNum: \t\t\t\t" + drNum);
            logger.WriteLn("cardNum: \t\t\t" + cardNum);
            logger.WriteLn("expDate: \t\t\t" + expDate);
            logger.WriteLn("cvc: \t\t\t\t" + cvc);
            logger.WriteLn("sum: \t\t\t\t" + sum);
            logger.WriteLn("type: \t\t\t\t" + type);
            logger.WriteLn("subPayload: \t\t" + subPayload);
            logger.WriteLn("refNum: \t\t\t" + refNum);
            logger.WriteLn("paramX: \t\t\t" + paramX);
            logger.WriteLn("token: \t\t\t\t" + token);
            logger.WriteLn("****************");
            logger.WriteLn("Request URL:" + client.getCmdUrl());
            logger.WriteLn("Request JSON:");
            logger.WriteLn(client.getRequestJson().toString());
            logger.WriteLn("Respond JSON:");
            logger.WriteLn(client.getRespondJson().toString());
        } catch (IOException ex) {
            Logger.getLogger(PelecardService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(logger != null)
                logger.close();
        }
    }
}
